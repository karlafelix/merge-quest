import sys


def add(first_arg, second_arg):
    return first_arg + second_arg


if __name__ == "__main__":
    first_operand = int(sys.argv[1])
    second_operand = int(sys.argv[2])

    print(f"add({first_operand}, {second_operand}) =", add(first_operand, second_operand))
